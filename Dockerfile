FROM python:3.8-alpine3.14

RUN apk add --update py3-pip

COPY ./requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

COPY ./back.py /usr/src/app/

CMD ["python3", "/usr/src/app/back.py"]
