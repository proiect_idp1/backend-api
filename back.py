import json
from flask import Flask, request
from flask.json import jsonify
from flask.wrappers import Response
from werkzeug.wrappers import response
import hashlib
from prometheus_client import Counter, Histogram
import pika
import os

app = Flask(__name__)
allContries = ["Ukraine", "Myanmar", "Nigeria"]

countryStats = {}

refugeeDb = {}

num_refugees_rq = Counter('total_refugees_requests', 'Total number of refugees requests')

R = [0 for _ in allContries]

V = [0 for _ in allContries]

'''
TODO METRICS :
               1. ratio volunteers/refugees per contry
               2. total number of refugees request

     LOGGING : Attempts of incorrect authentication with incorrect hash
               Successful request + receive in RabbitMQ


'''

def consume_callback(ch, method, properties, body):
    cmd = body.decode()
    print("Received %s", cmd)
    ch.basic_ack(delivery_tag=method.delivery_tag)
                

@app.route("/countries", methods = ["GET"])
def getCountries():
    response = jsonify(allContries)

    response.status = 200

    return response

@app.route("/refugee", methods = ["POST"])
def registerNewRefugee():

    info = json.loads(request.get_data().decode('utf-8'))

    global R,V

    if 'name' not in info or 'id' not in info or 'contactInfo' not in info:
        print("[ERR-1-INCORECT] : Required option not found in /refugee")
        response.status = 400
        return response


    plain = hashlib.sha256((info['name'] + info['id']).encode('utf-8')).hexdigest()

    if plain in refugeeDb:
        print(f"[ERR-2-CLONE] : Refugee id {plain} is already found in the database ")
        response.status = 401
        return response

    refugeeDb[plain] = info['contactInfo']


    index = allContries.index(info['contactInfo']['country'])
    R[index] = R[index] + 1

    if V[index] == 0:
        countryStats[info['contactInfo']['country']].observe(100000000)
    else:
        countryStats[info['contactInfo']['country']].observe(R[index] / V[index])

    response = jsonify({'cookie' : plain})

    print(plain)

    response.status = 200

    return response

@app.route("/refugee/<string:hash>", methods = ['POST'])
def refugeeAddHelp(hash : str):

    print(request.get_data())
    
    if hash not in refugeeDb:
        
        print('[ERR-3-INVALID] Hash provided not existing')
        return 

    num_refugees_rq.inc()

    # TODO add request to the queue of the contry present in contactInfo
    conn_mq = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))

    channel = conn_mq.channel()

    channel.queue_declare(queue=refugeeDb[hash]['country'], durable=True)

    channel.basic_publish('', refugeeDb[hash]['country'], request.get_data(), properties=pika.BasicProperties(delivery_mode=2))

    conn_mq.close()

    print('[OK-1-REQ_Q] : Correct request published')

    return "Correct request"



@app.route("/volunteer", methods = ['POST'])
def registerVolunteer():

    info = json.loads(request.get_data().decode('utf-8'))

    if 'name' not in info or 'country' not in info:
        response.status = 403
        return response

    global R,V

    print(info)

    index = allContries.index(info['country'])
    V[index] = V[index] + 1

    countryStats[info['country']].observe(R[index] / V[index])

    print(info['country'])

    # TODO register to the country queue specified in info

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='rabbitmq'))
    channel = connection.channel()
    channel.queue_declare(queue=info['country'], durable=True)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=info['country'], on_message_callback=consume_callback)
    channel.start_consuming()

    print('[OK-2-RES_Q] : Correct response published')

    return "Correct volunteer resp"


if __name__ == '__main__':

    for country in allContries:
        countryStats[country] = Histogram(country + '_RVRatio','Refugee/Volunteer ratio for ' + country, buckets=[0.01, 0.5, 0.7, 1, 1.5, 2])

    # app.run('0.0.0.0', os.environ.get('PORT'), debug=True)
    app.run('0.0.0.0', 8080, debug=True)

    

    
        